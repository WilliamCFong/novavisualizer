import os
import tempfile as tf

from django.core.files import File as FileWrapper
from django.utils import timezone

import data_manager.models as m
from datetime import timedelta, datetime
from copy import deepcopy
from math import ceil

class TimeIterator():

    """A class for iterating through time"""
    def __init__(self, cur_time=None):
        if cur_time is None:
            cur_time = timezone.now()
        else:
            cur_time = deepcopy(cur_time)
        self.date = cur_time

    def next(self, **kwargs):
        """Gets and sets the next timestep.

        :kwargs: Keyword arguments for timedelta.

        """
        time_step = self.date + timedelta(**kwargs)
        self.date += timedelta(**kwargs)
        return time_step

def create_log_dict(
    ip_addr='1.1.1.1',
    date=None,
    method='GET',
    referer='Referer String',
    byte_size=1000,
    response_code=200,
    url='/',
    from_job=None
    ):

    if date is None:
        date = timezone.now()
    elif isinstance(date, str):
        date = datetime.strptime(date, '%d/%b/%Y:%H:%M:%S %z')

    log_dict = {
        'remote_ip': ip_addr,
        'timestamp': date,
        'method': method,
        'referer': referer,
        'response_byte_size': byte_size,
        'response_code': response_code,
        'request_url': url,
        'user_agent_http': 'User Agent String',
        'from_job': from_job
    }

    return log_dict


def make_log(
        source='1.1.1.1',
        date=None,
        method='GET',
        referer='Referer',
        request_url='/url/',
        response_byte_size=100,
        response_code=200,
        user_agent_http='User Agent String'):

    if date is None:
        date = timezone.now()
    #else    #date = datetime.strptime(date, '%d/%b/%Y:%H:%M:%S %z')

    source = m.Source(ip_address=source)
    source.save()
    return m.Log(
            source=source,
            date=date,
            from_job=None,
            method=method,
            referer=referer,
            request_url=request_url,
            response_byte_size=response_byte_size,
            response_code=response_code,
            user_agent_http=user_agent_http)


def make_log_string(
        url,
        ip_addr='1.1.1.1',
        date=None,
        method='GET',
        response=200,
        response_byte_size=1000,
        referer='-',
        user_agent='useragent',
        return_as_bytes=True):
    if date is None:
        date = timezone.now()
    string = '%s - - [%s] "%s %s HTTP/1.1" %d %d "%s" "%s"\n' % \
             (
                 ip_addr,
                 date.strftime('%d/%b/%Y:%H:%M:%S %z'),
                 method,
                 url,
                 response,
                 response_byte_size,
                 referer,
                 user_agent
            )
    if return_as_bytes:
        return bytes(string, 'utf-8')
    else:
        return string


def check_source(ip_addr):
    if not m.Source.objects.filter(ip_address=ip_addr).exists():
        m.Source(ip_address=ip_addr).save()


def make_new_project_log(ip_addr, user, date, status=200):
    check_source(ip_addr)
    url = '/%s/projects/new/' % user
    log = m.Log.from_dict(create_log_dict(ip_addr, url=url, date=date, response_code=status))
    log.save()
    return log


def make_project_access(ip_addr, user, project_id, date, status=200):
    check_source(ip_addr)
    url = '/%s/projects/%d/' % (user, project_id)
    log = m.Log.from_dict(create_log_dict(ip_addr, url=url, date=date, response_code=status))
    log.save()
    return log


def make_remix_log(ip_addr, user, base_project, date, status=200):
    check_source(ip_addr)
    url = '/api/remix/%d/' % base_project
    log = m.Log.from_dict(create_log_dict(ip_addr, url=url, date=date, response_code=status))
    log.save()
    return log


def write_files():
    files = []
    ref = timezone.now()
    x = FileWrapper(tf.NamedTemporaryFile(mode='w+b'))
    x.write(make_log_string('/user1/projects/1/history/', ip_addr='1.1.1.1', date=ref+timedelta(seconds=1)))
    x.write(make_log_string('/user2/projects/2/history/', ip_addr='1.1.1.2', date=ref+timedelta(seconds=2)))
    x.write(make_log_string('/user3/projects/3/history/', ip_addr='1.1.1.3', date=ref+timedelta(seconds=3)))
    files.append(x)
    y = FileWrapper(tf.NamedTemporaryFile(mode='w+b'))
    y.write(make_log_string('/user1/projects/1/history/', ip_addr='1.1.2.1', date=ref+timedelta(seconds=4)))
    y.write(make_log_string('/user2/projects/2/history/', ip_addr='1.1.2.2', date=ref+timedelta(seconds=5)))
    y.write(make_log_string('/user3/projects/3/history/', ip_addr='1.1.2.3', date=ref+timedelta(seconds=6)))
    files.append(y)
    z = FileWrapper(tf.NamedTemporaryFile(mode='w+b'))
    z.write(make_log_string('/user1/projects/1/history/', ip_addr='1.1.3.1', date=ref+timedelta(seconds=7)))
    z.write(make_log_string('/user2/projects/2/history/', ip_addr='1.1.3.2', date=ref+timedelta(seconds=8)))
    z.write(make_log_string('/user3/projects/3/history/', ip_addr='1.1.3.3', date=ref+timedelta(seconds=9)))
    files.append(z)

    for f in files:
        f.seek(0)
    return files


def write_project_lifetime(project_id):
    x = FileWrapper(tf.NamedTemporaryFile(mode='w+b'))
    x.write(make_log_string(url='/user3/projects/1340/', ip_addr='2.2.2.2'))
    x.write(make_log_string(url='/user1/projects/new/', ip_addr='1.1.1.1'))
    x.write(make_log_string(url='/user3/projects/new/', ip_addr='2.2.2.2'))
    x.write(make_log_string(url='/junk/url/blah', ip_addr='1.1.1.1'))
    x.write(make_log_string(url='/user3/projects/1340', ip_addr='1.1.1.1'))
    x.write(make_log_string(url='/user1/projects/%d/', ip_addr='1.1.1.1'))
    x.seek(0)
    return x


def make_project_lifetime(
        project_id,
        owner,
        first_date=None,
        ip_address='1.1.1.1',
        n_access=4,
        time_delta_seconds=1,
        first_log_type='new',
        remix_project_id=23456):
    """Creates a new project lifetime.
    :project_id: The project's id.
    :owner: The owner of the project.
    :ip_address: An optional field where you can specify the address field of
    all the logs returned.
    :n_access: The number of access logs past the initial creator log.
    :time_delta_seconds: The amount of time between each log.
    :first_log_type: Can be 'new', 'remix', or 'none'. 'new' corresponds to
    creating a log through the ProjectLog action 'new'. 'remix' corresponds
    to creating a project through a remix api call on another project. 'none'
    will cause this function to ignore the creation log.
    :remix_project_id: Will only be used if the project creation type is 'remix'.
    :returns: A list of ProjectLogs and/or API_Calls that act on the project.
    """
    source = m.Source.objects.get_or_create(ip_address=ip_address)[0]
    project = m.Project(owner=owner, project_id=project_id, first_log=None)
    source.save()

    if first_date is None:
        first_date = TimeIterator()
    elif not isinstance(first_date, TimeIterator):
        first_date = TimeIterator(cur_time=first_date)

    base_info = {
            'source': source,
            'from_job': None,
            'method': 'GET',
            'referer': 'referer_str',
            'response_code': 200,
            'response_byte_size': 100,
            'user_agent_http': 'User Agent'}

    project_url_base = '/%s/projects/%d/' % (owner, project_id)

    logs = []

    # Create first access
    if first_log_type == 'new':
        new = m.Log(**base_info, request_url='/%s/projects/new/' % owner, date=first_date.date)
        new.save()
        logs.append(m.ProjectLog(log=new, project=None, action='new'))
    elif first_log_type == 'remix':
        # Create base project first
        try:
            remix_base = m.Project.objects.get(project_id=remix_project_id)
        except m.Project.DoesNotExist:
            remix_base = m.Project(project_id=remix_project_id, owner='remixer')
            base_log = m.Log(**base_info, request_url='/remixer/projects/new/', date=first_date.date - timedelta(minutes=100))
            base_log.save()
            remix_base.first_log = base_log
            base_pl = m.ProjectLog(log=base_log, action='new', project=remix_base)
            remix_base.save()
            base_pl.save()

        new = m.Log(**base_info, request_url='/api/remix/%d/' % remix_project_id, date=first_date.date)
        new.save()
        logs.append(m.API_Call(log=new, project=remix_base, call_type='remix'))
    elif first_log_type == None:
        # Do nothing
        pass
    else:
        raise ValueError('Only \'new\', \'remix\', or None are allowed.')

    # Create first access
    for i in range(1, n_access + 1):
        date = first_date.next(seconds=time_delta_seconds)
        log = m.Log(**base_info, request_url=project_url_base, date=date)
        log.save()
        logs.append(m.ProjectLog(log=log, project=project, action='edit'))

    [pl.save() for pl in logs]
    project.first_log = logs[1].log
    project.save()

    return logs


def write_logs(logs, clean_db=False):
    """Writes logs back into an apache log file. This is to be used for testing
    purposes.
    :logs: The logs to write.
    :clean_db: If true, the test_database will be erased.
    :return: A NamedTemporaryFile descriptor.
    """
    f = tf.NamedTemporaryFile(mode='w+b')
    for log in logs:
        b = bytes(log.to_apache_str() + '\n', 'utf-8')
        f.write(b)

    if clean_db:
        for model in [m.Source, m.Log, m.Location, m.ProjectLog, m.API_Call, m.Remix, m.Project]:
            model.objects.all().delete()

    f.seek(0)

    return f


def make_and_save_project_lifetime(project_ids, time_ref=None, clean_db=False, n_files=1, **kwargs):
    """A wrapper function for calling make_project_lifetime and write_logs.
    This function does not allow for specifying owners. Instead each project
    will be given owner_N where N is the nth given project id.

    :project_ids: Project IDs to generate lifetimes for.
    :time_ref: A specified datetime start reference. If None then the current
    time will be used. All logs are in 1 second increments.
    :n_files: The number of files to return. This function will attempt to split
    the logs generated evenly across them. If the number of files exceeds the number
    of logs ValueError will be raised.
    :clean_db: Should all saved records (logs, remixes, etc) be removed?
    :**kwargs: Keyword arguments passed into write_project_lifetime.

    :returns: A list of file pointers.

    """

    if not isinstance(project_ids, list):
        project_ids = [project_ids]

    nth_project = 0
    logs = []
    time_iter = TimeIterator()

    for project_id in project_ids:
        owner = 'owner_%d' % nth_project
        new_logs = make_project_lifetime(project_id, owner, first_date=time_iter, **kwargs)
        logs += new_logs
        nth_project += 1

    if n_files > len(logs):
        # Impossible to fit logs into the specified number of files
        # return error
        raise ValueError('Number of files %d exceeds number of logs %d' % (n_files, len(logs)))

    max_file_length = ceil(len(logs) / n_files)
    log_partitions = [logs[x:x+max_file_length] for x in range(0, len(logs), max_file_length)]
    files = []

    for log_partition in log_partitions:
        f = write_logs([l.log for l in log_partition])
        files.append(f)

    if clean_db:
        for model in [m.Source, m.Log, m.Location, m.ProjectLog, m.API_Call, m.Remix, m.Project]:
            model.objects.all().delete()

    return files


def write_mangled_files(n_entries, blocksize=1024):
    f = FileWrapper(tf.NamedTemporaryFile(mode='w+b'))
    for _ in range(n_entries):
        f.write(os.urandom(blocksize))
    f.seek(0)
    files = [f]

    return files

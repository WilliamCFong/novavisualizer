from django.db import models

from data_manager.models import ProjectLog, LinkStep, Project
from data_manager.managers import RollbackManager

class NewAction(models.Model):

    """Encompasses the a log which creates a brand new project using the url
    /owner/project/new."""

    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    project_log = models.ForeignKey(ProjectLog, on_delete=models.CASCADE)
    linkstep = models.ForeignKey(LinkStep, on_delete=models.CASCADE)

    objects = RollbackManager()
    regardless_of_state = models.Manager()

function _(ele) {
    return document.getElementById(ele);
}

function uploadFiles() {
    var files = _("files").files;
    var url = _("logUpload").action;
    var formdata =  new FormData();
    formdata.append("files", files);
    var ajax = new XMLHttpRequest();

    ajax.upload.addEventListener("progress", progressHandler, false);
    ajax.addEventListener("load", completeHandler, false);
    ajax.addEventListener("error", errorHandler, false);
    ajax.addEventListener("abort", abortHandler, false);
    ajax.open("POST", url);

    ajax.send(formdata);
}

function progressHandler(event) {
    _("amount_loaded").innerHTML = "Uploaded " + event.loaded + " bytes of " + event.total;
    var percent = (event.loaded / event.total) * 100;
    _("progressBar").style = "width:" + Math.round(percent) + "%;";
    _("status").innerHTML = Math.round(percent) + "% uploaded...";
}

function completeHandler(event) {
    _("status").innerHTML = event.target.responeText;
    _("progressBar").value = 0;
}

function errorHandler(event) {
    _("status").innerHTML = "Server upload failed" + event;
}

function abortHandler(event) {
    _("status").innerHTML = "Upload aborted.";
}

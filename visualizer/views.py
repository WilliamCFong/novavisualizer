from collections import Counter

from bokeh.embed import components
from bokeh.models import ColumnDataSource
from bokeh.plotting import figure
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.db.models import Count
from django.db.models import Count
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render
from django_ajax.decorators import ajax

import data_manager.models as m
from explorer.forms.by_location import LocationFilterForm
from visualizer.utils import to_mercator


@login_required()
def index(request):
    context = {}
    context['n_logs'] = m.Log.objects.count()
    context['n_projects'] = m.Project.objects.count()
    context['n_locations'] = m.Location.objects.count()
    return render(request, 'visuals/index.html', context=context)


@login_required()
def logs(request):
    context = {}
    logs = m.Log.objects.all()
    paginator = Paginator(logs, 100)
    page = request.GET.get('page')

    context['logs'] = paginator.get_page(page)
    return render(request, 'visuals/logs.html', context=context)

@login_required()
def log(request, log_id):
    context = {}
    log = get_object_or_404(m.Log, pk=log_id)
    context['log'] = log

    return render(request, 'visuals/log.html', context=context)

@login_required()
def projects(request):
    context = {}
    projects = m.Project.objects.all()
    paginator = Paginator(projects, 100)
    page = request.GET.get('page')

    context['projects'] = paginator.get_page(page)
    return render(request, 'visuals/projects.html', context=context)

@login_required
def project(request, project_id):
    context = {}
    project = get_object_or_404(m.Project, project_id=project_id)
    context['project'] = project

    project_logs = m.ProjectLog.objects.filter(project=project)
    api_calls = m.API_Call.objects.filter(project=project)
    histo = {}
    histo['n_logs'] = project_logs.count()
    if histo['n_logs'] > 0:
        histo['labels'], histo['data'] = zip(*dict(Counter(project_logs.values_list('action', flat=True))).items())
        histo['labels'] = list(histo['labels'])
        histo['data'] = list(histo['data'])

    else:
        histo['labels'], histo['data'] = ([], [])

    context['project_log_dist'] = histo

    histo = {}
    histo['n_api'] = api_calls.count()
    if histo['n_api'] > 0:
        histo['labels'], histo['data'] = zip(*dict(Counter(api_calls.values_list('call_type', flat=True))).items())
        histo['labels'] = list(histo['labels'])
        histo['data'] = list(histo['data'])
    else:
        histo['labels'], histo['data'] = ([], [])
    context['api_call_dist'] = histo

    return render(request, 'visuals/project.html', context=context)


@ajax
@login_required
def get_project_children(request):
    response = {}
    project_id = request.GET.get('project')
    project = get_object_or_404(m.Project, project_id=project_id)

    children = m.Remix.objects.filter(base_project=project)
    children = [
        {'id': child.child_project.project_id, 'owner': child.child_project.owner} for child in children
    ]

    response['id'] = project.project_id
    response['owner'] = project.owner
    response['children'] = children

    return response

@ajax
@login_required
def get_project_logs(request):
    response = {}
    project_id = request.GET.get('project')
    project = get_object_or_404(m.Project, project_id=project_id)

    project_logs = m.ProjectLog.objects.filter(project=project)
    api_calls = m.API_Call.objects.filter(project=project)

    logs = {}
    log_types = []
    n_logs = 0

    for project_log in project_logs:
        log = {}
        type = project_log.action
        log['date'] = project_log.log.date
        log['size'] = project_log.log.response_byte_size

        try:
            logs[type].append(log)
        except KeyError:
            logs[type] = [log]
            log_types.append(type)
        finally:
            n_logs += 1

    for api_call in api_calls:
        log = {}
        type = api_call.call_type
        log['date'] = api_call.log.date
        log['size'] = api_call.log.response_byte_size

        try:
            logs[type].append(log)
        except KeyError:
            logs[type] = [log]
            log_types.append(type)
        finally:
            n_logs += 1
    response['logs'] = logs
    response['types'] = log_types
    response['n_logs'] = n_logs
    response['project_id'] = project_id

    return response

@ajax
@login_required
def get_project_locations(request):
    response = {}
    project_id = request.GET.get('project')
    project = get_object_or_404(m.Project, project_id=project_id)

    api_locations, p_log_locations = project.get_access_locations()
    response['api_locations'] = [loc.get_coordinates(reverse=True) for loc in api_locations]
    response['project_locations'] = [loc.get_coordinates(reverse=True) for loc in p_log_locations]

    return response

@ajax
@login_required
def local_activity(request):
    response = {}
    country = request.GET.get('country')
    region = request.GET.get('region')
    city = request.GET.get('city')


@login_required()
def locations(request):
    context = {}
    if request.method == 'POST':
        form = LocationFilterForm(request.POST, queryset=m.Location.objects.all())
        if form.is_valid():
            context['locations'] = form.qs.filter(
                latitude__isnull=False,
                longitude__isnull=False
            ).annotate(
                n_logs=Count('source__log')
            ).all()
    else:
        form = LocationFilterForm()

    context['form'] = form

    return render(request, 'visuals/locations.html', context=context)


@login_required()
def throughput(request):
    context = {}
    logs = m.Log.objects.filter(date__range=['2017-01-01', '2017-06-31']).all()



def helpme(request):
    context = {
        'logged_in': request.user.is_authenticated
    }
    return render(request, 'help.html', context=context)

def wiki(request):
    context = {}
    return render(request, 'wiki.html', context=context)

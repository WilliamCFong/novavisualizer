from django.contrib.auth.models import User
from django.core.files import File as FileWrapper
from django.test import TestCase

from data_manager.models import Job, NewAction
from data_manager.tasks import job_chunk_process, link_projects
from data_manager.tests.testutils import make_project_lifetime, write_logs, make_and_save_project_lifetime


class NewActionUnitTests(TestCase):

    """ Perform some unit tests on the construction of NewActions from
    log strings."""

    def test_newaction_from_log(self):
        """Tests that a newaction is created from the correct log url"""

        lifetime = make_project_lifetime(12345, 'owner')
        input_file = write_logs([l.log for l in lifetime])
        job = Job(
            user=User.objects.create_user(
                'someuser',
                'user@admin.com',
                'secret'),
            job_file=FileWrapper(input_file)
        )

        job_chunk_process([job.pk])

        self.assertTrue(NewAction.objects.filter(project=12345).exists())
        self.assertEqual(NewAction.objects.count(), 1)

    def test_newaction_against_remix(self):
        """Tests that new newaction is made if there are no correct log urls"""

        make_project_lifetime(12345, 'owner', first_log_type='remix')

        link_projects()

        self.assertFalse(NewAction.objects.filter(project=12345).exists())

    def test_newaction_consistency_on_multiple_files(self):
        """Tests newactions on multiple files"""

        proj_ids = [x for x in range(100)]
        files = make_and_save_project_lifetime(proj_ids, n_files=20)
        
        link_projects()

        self.assertSetEqual(
            set(proj_ids),
            set(NewAction.objects.all().values_list('project__pk', flat=True))
        )
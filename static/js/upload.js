document.getElementById("file-input").addEventListener("change", function() {
    var form_field = document.getElementById("id_files");
    var collectionHTML = "<table class=\"highlight\">";
    collectionHTML += "<thead><tr><th>Filename</th><th>Filesize</th></thead>";
    collectionHTML += "<tbody>";
    for (var i = 0; i < form_field.files.length; i++) {
        filename = form_field.files.item(i).name;
        size = form_field.files.item(i).size;
        collectionHTML += "<tr>";
        collectionHTML += "<td>" + filename + "</td>";
        collectionHTML += "<td>" + size + " bytes" + "</td>";
        collectionHTML += "</tr>";
    }
    collectionHTML += "</tbody></table>"
    document.getElementById("filelist").innerHTML = collectionHTML;
});

# Generated by Django 2.0.7 on 2018-08-23 14:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('data_manager', '0015_remove_project_parent'),
    ]

    operations = [
        migrations.AlterField(
            model_name='log',
            name='id',
            field=models.BigIntegerField(primary_key=True, serialize=False),
        ),
    ]

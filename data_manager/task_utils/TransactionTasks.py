from celery import Task
from django.db import transaction


class TransactionAwareTask(Task):
    """
    This class wraps celery's task in order to create asynchronous tasks which
    are aware of hits to databases and will wait until commits occur in order to
    prevent race conditions.
    """

    abstract = True

    def apply_async(self, *args, **kwargs):
        transaction.on_commit(
            lambda: super(TransactionAwareTask, self).apply_async(*args, **kwargs)
        )

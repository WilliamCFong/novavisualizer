import os
import tempfile as tf
from unittest.mock import call, patch

from datetime import timedelta
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.files import File as FileWrapper
from django.db.utils import IntegrityError
from django.test import TestCase, override_settings, Client
from django.urls import reverse
from django.utils import timezone


import data_manager.models as m
import data_manager.tests.testutils as tu
from data_manager.tasks import job_chunk_process


class JobUnitTests(TestCase):

    def test_job_creation(self):
        user = User.objects.create_user('admin' 'admin@admin.com', 'secret')
        f = tf.NamedTemporaryFile(mode='w+b')
        test_str = 'Oh boy I hope this was encoded correctly. ¯\_(ツ)_/¯'
        f.write(bytes(test_str, 'utf-8'))
        f.seek(0)

        job = m.Job(job_file=FileWrapper(f), user=user)
        assert job.user == user
        assert job.job_file.read() == bytes(test_str, 'utf-8')
        assert job.status == m.Job.UNPROCESSED

    @override_settings(MEDIA_ROOT=tf.gettempdir())
    def test_job_set_finish(self):
        user = User.objects.create_user('admin', 'admin@admin.com', 'secret')
        job = m.Job(user=user, job_file=FileWrapper(tf.NamedTemporaryFile(mode='w+b')))
        job.save()

        job_chunk_process([job.pk])

        job = m.Job.objects.get(pk=job.pk)

        self.assertEqual(job.status, m.Job.FINISHED)


    @override_settings(MEDIA_ROOT=tf.gettempdir())
    def test_job_processing(self):
        files = tu.write_files()
        user = User.objects.create_user('admin', 'admin@admin.com', 'secret')
        jobs = [m.Job(user=user, job_file=f) for f in files]

        [job.save() for job in jobs]

        job_chunk_process([job.pk for job in jobs])


        addresses = ['1.1.1.1', '1.1.1.2', '1.1.1.3', '1.1.2.1', '1.1.2.2',
                     '1.1.2.3', '1.1.3.1', '1.1.3.2', '1.1.3.3']

        for addr in addresses:
            self.assertTrue(m.Source.objects.filter(ip_address=addr).exists())
            #self.assertTrue(m.Location.objects.filter(source=addr).exists())

        for job in jobs:
            self.assertEqual(m.Log.objects.filter(from_job=job.pk).count(), 3)

        self.assertEqual(m.Log.objects.count(), 9)
        self.assertEqual(m.Project.objects.count(), 3)
        self.assertEqual(m.Project.objects.get(project_id=1).owner, 'user1')

    @override_settings(MEDIA_ROOT=tf.gettempdir())
    def test_for_job_collisions(self):
        user = User.objects.create_user('someuser', 'user@admin.com', 'secret')
        now = timezone.now()
        original = tu.make_project_lifetime(
            12345,
            'owner',
            first_date=now,
            time_delta_seconds=100)
        collider = tu.make_project_lifetime(
            11111,
            'owner',
            first_date=now+timedelta(seconds=1))
        orig_file = tu.write_logs([l.log for l in original])
        collider = tu.write_logs([l.log for l in collider], clean_db=True)

        first_job = m.Job(user=user, job_file=FileWrapper(orig_file))
        first_job.save()

        # Process job
        job_chunk_process([first_job.pk])

        first_job.refresh_from_db()
        with self.assertRaises(ValidationError):
            colliding_job = m.Job(user=user, job_file=FileWrapper(collider))
            colliding_job.save()


class JobIntegrationTests(TestCase):
    @patch('data_manager.tasks.job_chunk_process.apply_async')
    @override_settings(MEDIA_ROOT=tf.gettempdir())
    def test_job_file_upload(self, mock):
        c = Client()
        c.force_login(User.objects.create_user('admin' 'admin@admin.com', 'secret'))
        logs = tu.make_project_lifetime(12345, 'owner', ip_address='1.1.1.1')
        f = tu.write_logs([l.log for l in logs], clean_db=True)
        result = c.post(reverse('data-upload'), {'files': [f]}, follow=True)

        self.assertTrue(mock.called)

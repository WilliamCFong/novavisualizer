from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.views.generic.edit import FormView

import data_manager.tasks as background_task
from data_manager.utils import download_files, link_projects, get_unlinked_projects

from .forms import LogFileForm
from .models import Job, Log, Project, NewAction, Remix, LinkStep


@login_required()
def index(request):
    context = {}
    # Load Contexts
    possible_processes = ['is_processing_api', 'is_processing_project_logs', 'is_linking_projects']
    for poss in possible_processes:
        try:
            context[poss] = request.session['poss']
        except KeyError:
            request.session[poss] = False
            context[poss] = request.session[poss]

    return render(request, 'data/index.html', context=context)


@login_required()
def upload(request):
    if request.method == 'POST':
        # A lot of inserts can be generated from a fileupload. Ensure that
        # these are placed into a single commit for integrity.
        form = LogFileForm(request.POST, request.FILES)
        if form.is_valid():
            files = request.FILES.getlist('files')
            jobs = [job.pk for job in download_files(files, request.user)]
            background_task.job_chunk_process.apply_async(args=[jobs])

            return HttpResponseRedirect(reverse('job-list'))
        else:
            return HttpResponseRedirect(reverse('data-upload'))

    # Request method is GET
    form = LogFileForm()
    return render(request, 'data/upload.html', {'form': form})

@login_required()
def link_projects(request):
    if request.method == 'POST':
        linkstep = background_task.link()
        return HttpResponseRedirect(reverse('linking-progress', args=(linkstep,)))
    
    context = {}
    return render(request, 'data/linker.html', context=context)

@login_required()
def link_progress(request, linker_pk):
    linkstep = get_object_or_404(LinkStep, pk=linker_pk)
    newactions = NewAction.objects.filter(linkstep=linkstep)
    remixes = Remix.objects.filter(linkstep=linkstep)

    return render(request, 'data/link_progress.html', context={
        'newactions': newactions,
        'remixes': remixes,
        'linkstep': linker_pk})
    

@login_required()
def jobs(request):
    context = {}
    context['jobs'] = Job.objects.all()
    return render(request, 'data/jobs.html', context=context)

@login_required()
def job(request, job_id):
    context = {}
    context['job'] = Job.objects.get(pk=job_id)
    context['n_logs'] = Log.objects.filter(from_job=job_id).count()
    return render(request, 'data/job.html', context=context)

@login_required()
def delete_job(request, job_id):
    target = Job.objects.get(pk=job_id)
    target.delete()
    return HttpResponseRedirect(reverse('job-list'))

@login_required()
def delete_all_projects(request):
    Project.objects.all().delete()
    return HttpResponseRedirect(reverse('data-index'))

class LogFileView(FormView):
    form_class = LogFileForm
    template_name = 'data/form.html'

    def post(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

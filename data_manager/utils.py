#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
import re
from datetime import datetime
from functools import partial
from tempfile import TemporaryFile

from dateutil.parser import parse
from django.core.files.uploadhandler import FileUploadHandler
from django.db.utils import DataError

import data_manager.models as m

logger = logging.getLogger(__name__)


def is_integer(string):
    try:
        int(string)
        return True
    except ValueError:
        return False


def get_nested_item(dict_like, path_str):
    paths = path_str.split('__')
    item = dict_like
    try:
        for path in paths:
            item = item[path]
    except KeyError:
        return None
    return item


def download_file(filestream, user):
    """This method reads a client upstream and processes it into a Job instance.
    These job instances will later be processed into logs, sources, etc.
    """
    new_job = m.Job(job_file=filestream, user=user)
    new_job.save()

    return new_job


def download_files(files, user):
    """This method handles multiple file uploads from a user"""
    for upload in files:
        yield download_file(upload, user)


def chunkify_jobs(job_ids, chunksize=100000):
    jobs = m.Job.objects.filter(pk__in=job_ids, status=m.Job.UNPROCESSED)
    extract = re.compile(r'([(\d.)]+) - - \[(.*?)\] \"(.*?)\" (\d+) (\d+) \"(.*?)\" \"(.*?)\"')
    chunk_n = 0
    for job in jobs:
        chunk = []
        job.status = m.Job.PROCESSING
        job.save()
        for line in job.job_file:
            try:
                result = extract.match(str(line, 'utf-8'))
            except UnicodeDecodeError:
                # Ignore result
                continue
            if result:
                chunk.append(result.groups())
            if len(chunk) == chunksize:
                logger.debug('Yielding chunk #%d' % chunk_n)
                yield (chunk, job)
                chunk_n += 1
                chunk = []
        logger.debug('Flushing remainder of chunk #%d' % chunk_n)
        chunk_n += 1
        # Flush remainder of chunk
        yield (chunk, job)

    for job in jobs:
        job.status = m.Job.FINISHED
        job.message = "Job finished!"
        job.save()


def conv_raw_to_dict(raw_tuple):
    """Converts a parsed tuple of a log string into a dictionary"""
    log = {}
    log['source'] = raw_tuple[0]
    log['date'] = datetime.strptime(raw_tuple[1], '%d/%b/%Y:%H:%M:%S %z')
    try:
        request_elements = raw_tuple[2].split(' ')
        log['method'] = request_elements[0]
        log['request_url'] = request_elements[1]
    except IndexError:
        log['method'] = ''
        log['request_url'] = ''

    log['response_code'] = raw_tuple[3]
    log['response_byte_size'] = raw_tuple[4]
    log['referer'] = raw_tuple[5] if raw_tuple[5] != '-' else ''
    log['user_agent_http'] = raw_tuple[6]

    return log


def construct_log_info(log):
    # Only process if the log has a successful 200 status.
    if not (200 <= int(log.response_code) < 300):
        return None

    split_url = log.request_url\
                   .split('&')[0]\
                   .split('?')[0]\
                   .split('/')  # Ignore HTTP parameters
    info_array = []

    for x in split_url:
        if len(x) > 0:
            info_array.append(x)

    info = {}
    try:
        if info_array[0] == 'api':
            # Api call
            info['call_type'] = info_array[1]
            info['project_revision'] = int(info_array[3]) if len(info_array) == 4 else 0
            info['project'] = int(info_array[2])
            info['owner'] = None
        elif info_array[1] == 'projects' and info_array[2] == 'new':
            # ProjectLog with no associated project
            info['owner'] = info_array[0]
            info['action'] = 'new'
            info['project'] = None
            info['project_revision'] = 0
        elif info_array[1] == 'projects' and len(info_array) == 3:
            info['owner'] = info_array[0]
            info['project'] = int(info_array[2])
            info['action'] = 'edit'
            info['project_revision'] = 0
        elif info_array[1] == 'projects' and is_integer(info_array[2]):
            # Process Project Log
            info['owner'] = info_array[0]
            info['project'] = int(info_array[2])
            if is_integer(info_array[3]):
                info['project_revision'] = int(info_array[3])
                info['action'] = 'edit'
            else:
                info['project_revision'] = 0
                info['action'] = info_array[3]
    except IndexError:
        # Assume something very bad happened, like a mangled url.
        return None

    except ValueError:
        # A cast likely failed, suspect mangled url.
        return None


    return None if info == {} else info


def resolve_project(project_id, log, project_cache, owner=None):
    """Resolves a project from cache

    :project_id: The project id to attempt to resolved.
    :log: The log object encompassing the project.
    :project_cache: The cache of currently defined projects.
    :returns: A tuple if a project was resolved. The first element of the tuple
    is the resolved project, and the second a boolean signifying a cache hit.
    If no project could be properly resolved then the first element will be
    None and it will be considered a cache miss (false).

    """
    if project_id is None:
        return (None, False)

    if owner is None:
        owner = m.Project.unknown_owner

    # Attempt to resolve project from cache
    try:
        project = project_cache[project_id]
        cache_hit = True
        # Check to see if we need to update project
        try:
            if project.first_log.date > log.date:
                # Newer log found acting on project
                project.first_log = log
        except AttributeError:
            # Error in getting first log. Likely because the project no longer
            # correctly points to a log, fix that link with this log
            project.first_log = log

        # Check to see if we need to check for new ownership
        if project.owner == m.Project.unknown_owner:
            project.owner = owner

    except KeyError:
        # No project was found, we'll need to make one
        project = m.Project(project_id=project_id, owner=owner, first_log=log)

        # Update cache
        project_cache[project_id] = project
        cache_hit = False

    return (project, cache_hit)


def get_window(queryset, window_size, window_index, order_by):
    """Gets a window of some models and returns them. Requires an ordering.

    :queryset: The queryset to break into windows.
    :window_size: The size of the window to return.
    :window_index: The index of the window to obtain.
    :order_by: The string specifying which field to sort the window by. This is
    required in order to guarantee that a window with the same parameters will
    return the same window contents.
    :returns: A 'window' of the entire model dataset.

    """

    i_min = window_size * window_index
    i_max = window_size + i_min

    return queryset.order_by(order_by)[i_min:i_max]


def constrict_log_list(log_list, date, ip_address, owner):
    """This function finds elements within the log list that appear
    at or after the given date. If an ip_address or owner is given then the log
    must also have the same value.

    :log_list: The list of logs to search
    :date: The cutoff date.
    :ip_address: The ip address to match.
    :owner: The owner to match against, logs which are API logs will still
    match.
    :returns: A sublist of log objects.

    """
    result = []
    for log in log_list:
        if log.log.date > date:
            # Log came in after the given date. Ignore it from the list
            continue
        if ip_address and log.log.source.ip_address != ip_address:
            # Log is from a different source, if an ip_address was not given
            # then this check is ignored
            continue
        if owner:
            # Log may not have an owner field
            try:
                if log.log.owner != owner:
                    continue
            except AttributeError:
                # Log is an API Call
                pass
        # Log passed all our checks, add it to the resultant
        result.append(log)

    return result


def get_unlinked_projects():
    """Returns a queryset of unlinked projects. This still requires a large
    hit to the database as the query involves finding all newactions and
    remixes
    
    :returns: A queryset of unlinked projects."""


    newactions = m.NewAction.objects.all()
    remixes = m.Remix.objects.all()
    remixed_projects = remixes.values_list('child_project__pk', flat=True)
    new_projects = newactions.values_list('project__pk', flat=True)

    linked_project_pks = list(new_projects) + list(remixed_projects)

    unlinked_projects = m.Project.objects.exclude(pk__in=linked_project_pks)

    return unlinked_projects

def link_projects(linkstep_id=None, window_size=1000000):
    """Links projects to create actions.

    :linkstep_id: The id to the current linkstep context for rollback
    capability. If None, then a linkstep object will be created.
    :window_size: The amount of logs to load into memory at a time.

    """

    if linkstep_id is None:
        linkstep = m.LinkStep()
    else:
        linkstep = m.LinkStep.objects.get(pk=linkstep_id)
    linkstep.save()

    # TODO Add ip context parameter and move to tasks so this can be done
    # somewhat asynchronously.

    unlinked_projects = get_unlinked_projects()

    project_logs_to_link = m.ProjectLog.objects\
                                    .exclude(pk__in=m.NewAction.objects.all())\
                                    .filter(action='new')

    api_calls_to_link = m.API_Call.objects\
                                .exclude(pk__in=m.Remix.objects.all())\
                                .filter(call_type='remix')
    # Evaluate logs
    project_logs_to_link = list(project_logs_to_link)
    api_calls_to_link = list(api_calls_to_link)
    logs = project_logs_to_link + api_calls_to_link

    # Sort logs so they are in date order
    logs = sorted(logs, key=lambda x: x.log.date)

    unresolved_projects = []
    new_remixes = []
    new_newactions = []
    project_updates = []

    for project in unlinked_projects:
        logger.info('Attempting to link project: %d' % project.project_id)
        owner_context = project.owner if project.owner != m.Project.unknown_owner else None
        ip_context = project.first_log.source.ip_address
        datecheck = project.first_log.date

        possible_logs = constrict_log_list(logs, datecheck, ip_context, owner_context)
        if not possible_logs:
            # No possible match
            unresolved_projects.append(project)
            continue

        # Grab the earliest match
        target = possible_logs[0]

        # Remove log from further matches
        logs.remove(target)

        # Two possible courses of action:
        # Target Log is either an API Remix Call or a Project Log
        if isinstance(target, m.API_Call):
            # Create remix object
            remix = m.Remix(
                base_project=target.project,
                child_project=project,
                api_call=target,
                linkstep=linkstep)
            new_remixes.append(remix)
        else:
            # Log is a ProjectLog
            newaction = m.NewAction(
                project=project,
                project_log=target,
                linkstep=linkstep)
            new_newactions.append(newaction)

        # Finally update the project first log
        project.first_log = target.log
        project_updates.append(project)

    # Finally update all objects
    logger.info('Commiting Remixes')
    m.Remix.objects.bulk_create(new_remixes)
    logger.info('Commiting NewActions')
    m.NewAction.objects.bulk_create(new_newactions)
    for project in project_updates:
        project.save()

    return linkstep_id

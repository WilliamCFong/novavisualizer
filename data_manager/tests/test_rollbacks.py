from django.contrib.auth.models import User
from django.test import TestCase

from data_manager.models import Job, NewAction, Remix, LinkStep
from data_manager.tests.testutils import make_and_save_project_lifetime
from data_manager.tasks import link_projects

class RollbackUnitTests(TestCase):

    """Performs unit tests on maintaining integrity and consistency across
    user rollback actions."""

    def test_single_rollback(self):
        # Create lifetimes
        # Check that proper model fields are set
        # Rollback, check that relevant models are reset
        proj_ids = [x for x in range(10)]
        user = User.objects.create_user('user', 'user@admin.com', 'secret')
        make_and_save_project_lifetime(proj_ids, n_files=5)

        link_projects()

        self.assertSetEqual(
            {x for x in range(10)},
            set(NewAction.objects.all().values_list('project__pk', flat=True))
        )

        LinkStep.objects.first().rollback()

        for proj_id in proj_ids:
            self.assertFalse(NewAction.objects.filter(project=proj_id).exists())

    def test_rollback_undo(self):
        # Perform rollback twice and check that model fields are back to their
        # initial states.
        proj_ids = [x for x in range(10)]
        self.test_single_rollback()
        # Rollback as occured, reapply
        LinkStep.objects.first().rollback()

        for proj_id in proj_ids:
            self.assertTrue(NewAction.objects.filter(project=proj_id).exists())
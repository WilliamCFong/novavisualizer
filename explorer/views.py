from bokeh.embed import components
from bokeh.layouts import Column
from bokeh.plotting import figure
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.db.models import Count
from django.shortcuts import render

import data_manager.models as m
#rom data_manager.forms import (API_CallFilterForm, Log_FilterForm,
#                                Project_FilterForm, ProjectLog_FilterForm)
#
from explorer.forms import (API_CallFilterForm, Log_FilterForm,
                            Project_FilterForm, ProjectLog_FilterForm)


@login_required()
def index(request):
    context = {}
    context['n_logs'] = m.Log.objects.count()
    context['n_projects'] = m.Project.objects.count()
    context['n_api_calls'] = m.API_Call.objects.count()
    context['n_project_logs'] = m.ProjectLog.objects.count()

    return render(request, 'explorer/index.html', context=context)

@login_required()
def project_view(request, project_id):
    context = {}
    context['project'] = m.Project.objects.get(project_id=project_id)
    context['project_logs'] = m.ProjectLog.objects.filter(project=context['project']).order_by('log__date')
    context['api_calls'] = m.API_Call.objects.filter(project=context['project']).order_by('log__date')
    try:
        context['parent'] = m.Remix.objects.get(child_project=context['project']).base_project
    except m.Remix.DoesNotExist:
        context['parent'] = None

    context['children'] = m.Remix.objects.filter(base_project=context['project'])

    return render(request, 'explorer/project.html', context=context)

@login_required()
def projects(request):
    context = {}

    form = Project_FilterForm(request.GET, queryset=m.Project.objects.all())

    if form.is_valid():
        query = form.qs
        context['order_method'] = form.get_order_label()
    else:
        query = m.Project.objects.exclude(project_id=-1)
        #context['order_method'] = None

    # Paginate
    query = Paginator(query, 20)
    page = request.GET.get('page')
    query = query.get_page(page)

    context['projects'] = query
    context['form'] = form

    return render(request, 'explorer/projects.html', context=context)


@login_required()
def project_logs(request):
    context = {}

    form = ProjectLog_FilterForm(request.GET, queryset=m.ProjectLog.objects.all())

    if form.is_valid():
        query = form.qs
    else:
        query = m.ProjectLog.objects.all()

    # Paginate
    query = Paginator(query, 20)
    page = request.GET.get('page')
    query = query.get_page(page)

    context['project_logs'] = query
    context['form'] = form

    return render(request, 'explorer/project_logs.html', context=context)


@login_required()
def api_calls(request):
    context = {}
    form = API_CallFilterForm(request.GET, queryset=m.API_Call.objects.all())

    if form.is_valid():
        query = form.qs
    else:
        query = m.API_Call.objects.all()

    # Paginate
    query = Paginator(query, 20)
    page = request.GET.get('page')
    query = query.get_page(page)

    context['api_calls'] = query
    context['form'] = form
    return render(request, 'explorer/api_calls.html', context=context)

@login_required()
def logs(request):
    context = {}
    #logs = Log_Filter(request.GET, queryset=m.Log.objects)
    form = Log_FilterForm(request.GET, queryset=m.Log.objects.all())

    if form.is_valid():
        query = form.qs
    else:
        query = m.Log.objects.all()

    query = Paginator(query, 20)
    page = request.GET.get('page')
    query = query.get_page(page)

    context['logs'] = query
    context['form'] = form
    return render(request, 'explorer/logs.html', context=context)

from __future__ import absolute_import, unicode_literals

import os

from celery import Celery

# Initialize Celery using NovaVisualizer settings
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'NovaVisualizer.settings')
app = Celery('NovaVisualizer')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))

from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='explorer-index'),
    path('api_calls', views.api_calls, name='explorer-api-calls'),
    path('logs', views.logs, name='explorer-logs'),
    path('project/<int:project_id>', views.project_view, name='explorer-project-view'),
    path('projects', views.projects, name='explorer-projects'),
    path('project_logs', views.project_logs, name='explorer-project-logs')
]

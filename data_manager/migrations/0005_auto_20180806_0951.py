# Generated by Django 2.0.7 on 2018-08-06 13:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('data_manager', '0004_merge_20180801_1846'),
    ]

    operations = [
        migrations.AlterField(
            model_name='location',
            name='country',
            field=models.CharField(max_length=100, null=True),
        ),
    ]

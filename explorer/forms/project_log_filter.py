from django_filters import FilterSet, OrderingFilter

from data_manager.models import ProjectLog


class ProjectLog_FilterForm(FilterSet):

    class Meta:
        model = ProjectLog
        fields = {
            'project__project_id': ['exact'],
            'action': ['exact', 'contains'],
            'log__response_code': ['exact']
        }


    ordering = OrderingFilter(
        fields=['log__date', 'action'],
        field_labels={
            'log__date': 'Log Date',
            'action': 'Action'
        }
    )

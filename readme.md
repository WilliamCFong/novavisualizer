###Requirements
erlang
rabbitmq

## Installation
First ensure that you have a Python 3 installation (at least python version 3.5).
For virtual environments I recommend using `pipenv` as a virtual environment
manager. In this way after `pip install pipenv` you can just run the following in
a terminal.

```shell
pipenv --python {your version}
pipenv shell
pip install -r requirements.txt
```

This will create a virtual environment and install all the requisite python
packages used by this project.

###Database
Postgresql is the SQL flavor used by this project. Install it, for MacOS and
Windows this should be a downloadable installer. For linux users it's probably
best to install it using whatever package manager your OS uses.

Once installed create a database called `nova`. For linux and MacOS this should
be easy as `createdb nova`. For windows, use the pg_admin utility to make this
database.

Once you create the database make sure that you've activated your virtual
environment `pipenv shell` (make sure that you only run once!). From there
we can migrate the database to the current known schema by `python manage.py migrate`.

Be sure to update the user credentials in `NovaVisualizer.settings.py` for the
database connection (under the field DATABASES).

###Message Broker
The large amount of data processing requires that we use the Python Package
`Celery` which requires the use of a Async. message broker. While any broker
should work, this readme will document the use of `rabbitmq`.

For MacOS and Windows this should be a downloadable installer. For linux:
after you install the backend make sure that it's service hooks are loaded.

Once installed we need to create the rabbitmq user. In a terminal run
`sudo rabbitmqctl add_user {USER} {PASSWORD}`

Create a virtual host
`sudo rabbitmqctl add_vhost {VHOST}`

Create a tag for adding user permissions
`sudo rabbitmqctl set_user_tags {USER} {TAG}`

Finally add blanket tag permissions
`sudo rabbitmqctl set_permissions -p {VHOST} {USER} ".*" ".*" ".*"`

Once all these have been set. Edit the `NovaVisualizer/settings.py` and locate
the `CELERY_BROKER_URL` and set it to the format of `amqp://{USER}:{PASSWORD}@localhost:5672/{VHOST}`

For window urls it should just be `amqp://localhost` but will require more
configuration. See the Celery docs

For more relevant docs on this see http://docs.celeryproject.org/en/latest/getting-started/brokers/rabbitmq.html#broker-rabbitmq

###InitialUser
In a terminal and in the proper virtual environment setup the initial user
with `python manage.py createsuperuser` and follow the prompts.


# Using it!
### Running the Django Server
To run the server make sure you're still in the virtual environment and run:
`python manage.py runserver`
If the base Django configuration is right it should work right away.

### Running celery backend
Load another terminal and go to the project directory and activate the virtual
environment.
run `celery -A NovaVisualizer worker --loglevel=info` in order to have Celery
listen for jobs to complete.

To upload logs go to `localhost:8000/data/upload`. And upload some logs.
If rabbitmq was not configured correctly then the server will crash. Make
sure that before you reupload data after fixing that you go to `/data/jobs`
and delete any orphan jobs that might have been created.

from django.test import TestCase
from django.core.exceptions import ValidationError
from data_manager.models import BotFilter, Job
import mmh3
from data_manager.tests.testutils import make_and_save_project_lifetime
from data_manager.utils import chunkify_jobs
from django.contrib.auth.models import User


class BotFilterUnitTests(TestCase):
    """Tests the creation and behavior of BotFilters"""

    def test_botfilter_uniqueness(self):
        duplicate_name = 'UserAgent'
        original = BotFilter(user_agent=duplicate_name)
        original.save()
        with self.assertRaises(ValidationError):
            BotFilter(user_agent=duplicate_name).save()

    def test_generate_hash(self):
        bot_filter = BotFilter(user_agent='user agent')
        bot_filter.save()
        self.assertTrue(mmh3.hash_bytes('user agent'), bot_filter.bot_hash)


class BotFilterIntegrationTests(self):
    """Tests the integration of BotFilters to the other models.
    For example, no ProjectLog and API models should be generated
    from logs which have been identified as originating from bots.
    """

    def test_no_project_logs(self):
        bot_str = 'Look at me I\'m a bot!'
        f = make_and_save_project_lifetime([12345], clean_db=True, user_agent=bot_str)[0]

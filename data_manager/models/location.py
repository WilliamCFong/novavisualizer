from django.db import models
from django.utils import timezone

from data_manager.models import Source


class Location(models.Model):
    source = models.OneToOneField(Source, on_delete=models.CASCADE, unique=True)
    country = models.CharField(max_length=100, null=True)
    region = models.CharField(max_length=100, null=True)
    city = models.CharField(max_length=100)
    latitude = models.FloatField(null=True)
    longitude = models.FloatField(null=True)
    accuracy = models.FloatField(null=True)

    last_updated = models.DateTimeField()

    def get_coordinates(self, reverse=False):
        """Gets the coordinates of the location (lat, lon).

        :reverse: If true then the coordinates are returned in (lon, lat)
        format. This is useful for visualization libraries which use (x, y)
        projection techniques.

        """

        if reverse:
            return (self.longitude, self.latitude)
        return (self.latitude, self.longitude)

    def save(self, *args, **kwargs):
        """This overrided save function ensures that the field 'last_updated'
        always reflects whenever the model is saved."""

        self.last_updated = timezone.now()

        super().save(*args, **kwargs)

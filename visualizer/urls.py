from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='visualizer-index'),
    path('logs', views.logs, name='visualizer-log-list'),
    path('log/<int:log_id>', views.log, name='visualize-log'),
    path('projects', views.projects, name='visualizer-project-list'),
    path('project/<int:project_id>', views.project, name='visualize-project'),
    path('locations', views.locations, name='visualizer-locations'),
    path('ajax/project', views.get_project_children, name='ajax-project-tree'),
    path('ajax/project-logs', views.get_project_logs, name='ajax-project-logs'),
    path('ajax/project-locations', views.get_project_locations, name='ajax-project-locations')
]

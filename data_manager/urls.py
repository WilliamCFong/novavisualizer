from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='data-index'),
    path('upload/', views.upload, name='data-upload'),
    path('linker/', views.link_projects, name='linker'),
    path('linking_progress/<int:linker_pk>', views.link_progress, name='linking-progress'),
    path('jobs/', views.jobs, name='job-list'),
    path('job/<int:job_id>', views.job, name='view-job'),
    path('jobs/delete/<int:job_id>', views.delete_job, name='delete-job'),
    path('delete_all/projects', views.delete_all_projects, name='delete-all-projects')
]

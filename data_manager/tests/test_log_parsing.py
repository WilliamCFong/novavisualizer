from django.test import TestCase

from data_manager.utils import construct_log_info
from data_manager.models import Log
from data_manager.tests.testutils import make_log


class InfoConstructionUnitTest(TestCase):

    def test_ignore_urls(self):
        url = {'request_url': '/this/url/is_ignored'}
        result = construct_log_info(make_log(**url))
        self.assertEqual(result, None)

    def test_api_info_construction(self):
        url = {'request_url': '/api/blocks/12345/'}
        result = construct_log_info(make_log(**url))
        self.assertEqual(result['call_type'], 'blocks')
        self.assertEqual(result['project'], 12345)
        self.assertEqual(result['project_revision'], 0)
        self.assertEqual(result['owner'], None)

    def test_api_with_revision(self):
        opt = {'request_url': '/api/blocks/12345/10'}
        result = construct_log_info(make_log(**opt))
        self.assertEqual(result['project_revision'], 10)

    def test_project_log_info_construction(self):
        opt = {'request_url': '/owner/projects/12345/'}
        result = construct_log_info(make_log(**opt))
        self.assertEqual(result['owner'], 'owner')
        self.assertEqual(result['project'], 12345)
        self.assertEqual(result['project_revision'], 0)
        self.assertEqual(result['action'], 'edit')

    def test_project_log_info_with_action(self):
        opt = {'request_url': '/owner/projects/12345/play/'}
        result = construct_log_info(make_log(**opt))
        self.assertEqual(result['owner'], 'owner')
        self.assertEqual(result['project'], 12345)
        self.assertEqual(result['project_revision'], 0)
        self.assertEqual(result['action'], 'play')

    def test_project_log_info_with_revision(self):
        opt = {'request_url': '/owner/projects/12345/10/'}
        result = construct_log_info(make_log(**opt))
        self.assertEqual(result['owner'], 'owner')
        self.assertEqual(result['project'], 12345)
        self.assertEqual(result['project_revision'], 10)
        self.assertEqual(result['action'], 'edit')

    def test_new_project_log(self):
        opt = {'request_url': '/owner/projects/new/'}
        result = construct_log_info(make_log(**opt))
        self.assertEqual(result['owner'], 'owner')
        self.assertEqual(result['project'], None)
        self.assertEqual(result['project_revision'], 0)
        self.assertEqual(result['action'], 'new')

    def test_leading_and_trailing_slash(self):
        trailing = {'request_url': '/api/blocks/100'}
        leading = {'request_url': 'api/blocks/100/'}
        both = {'request_url': 'api/blocks/100'}
        mangled = {'request_url': 'api//blocks///100//'}

        trailing_result = construct_log_info(make_log(**trailing))
        leading_result  = construct_log_info(make_log(**leading))
        both_result     = construct_log_info(make_log(**both))
        mangled_result   = construct_log_info(make_log(**mangled))

        self.assertEqual(trailing_result['call_type'], 'blocks')
        self.assertEqual(trailing_result['project'], 100)

        # Ignore log key
        trailing_result['log'] = None
        leading_result['log'] = None
        both_result['log'] = None
        mangled_result['log'] = None

        self.assertDictEqual(trailing_result, leading_result)
        self.assertDictEqual(trailing_result, both_result)
        self.assertDictEqual(trailing_result, mangled_result)

    def test_drop_html_parameters(self):
        param = {'request_url': '/owner/projects/12345/edit&2d=true'}
        query = {'request_url': '/owner/projects/12345/edit?j_querycallback=blah'}

        param_result = construct_log_info(make_log(**param))
        query_result = construct_log_info(make_log(**query))

        # Ignore log key
        param_result['log'] = None
        query_result['log'] = None

        self.assertEqual(param_result['owner'], 'owner')
        self.assertEqual(param_result['project'], 12345)
        self.assertEqual(param_result['project_revision'], 0)
        self.assertEqual(param_result['action'], 'edit')

        self.assertDictEqual(param_result, query_result)

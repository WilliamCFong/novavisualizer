from django.db import models

from data_manager.models import API_Call, Project, LinkStep
from data_manager.managers import RollbackManager

class Remix(models.Model):
    api_call = models.ForeignKey(API_Call, on_delete=models.CASCADE)
    base_project = models.ForeignKey(
        Project,
        on_delete=models.CASCADE,
        related_name='remixed_from_%(class)s'
    )
    child_project = models.ForeignKey(
        Project,
        on_delete=models.CASCADE,
        related_name='remixed_to_%(class)s'
    )

    linkstep = models.ForeignKey(LinkStep, on_delete=models.CASCADE)
    objects = RollbackManager()
    regardless_of_state = models.Manager()

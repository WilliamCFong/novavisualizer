import re
import logging

from dateutil.parser import parse
from django.db import models
from django.db.utils import DataError

from data_manager.models import Log, Source

logger = logging.getLogger(__name__)

class Project(models.Model):
    project_id = models.BigIntegerField(primary_key=True)
    owner = models.CharField(max_length=200)
    first_log = models.ForeignKey(Log, on_delete=models.CASCADE)

    unknown_owner = '--/UNKNOWN/--'

    def __str__(self):
        return '%d %s %d' % (self.project_id, self.owner, self.first_log.pk)

    @classmethod
    def generate_projects(cls, logs=None):
        """
        This method will create/update projects from all the existing logs in
        the system.
        """
        if isinstance(logs, Log):
            logs = [logs]
        elif logs is None:
            logs = Log.objects.order_by('date').all()

        for log in logs:
            result = cls.update_project_on_log(log)
            if result:
                yield result

    @classmethod
    def update_project_on_log(cls, log):
        """
        This method will attempt to make a project from a log. It is possible
        None will be returned if the Log url is not operating in Project namespaces
        """
        log_extract = r'/([\w\$\.\!\+\-\*\(\)\,]+)/projects/(\d+)/.*'
        api_extract = r'/api/\w+/(\d+)/.*'
        log_match = re.search(log_extract, log.request_url)
        api_match = re.search(api_extract, log.request_url)

        if log_match:
            username, proj_id = log_match.groups()
        elif api_match:
            proj_id = api_match.groups()[0]
            username = Project.unknown_owner
        elif int(log.response_code) >= 400:
            # Ignore errors
            return None
        else:
            # No match was found
            return None
        result = Project.update(username, proj_id, log)
        try:
            result.save()
        except DataError:
            logger.error('ERROR', log)
        return result

    @classmethod
    def update(cls, username, project_id, log):
        """
        This method updates a project. If no project exists at the specified
        id then one is created.
        """
        try:
            project = cls.objects.get(pk=int(project_id))
            if isinstance(log.date, str):
                log_time = parse(log.date)
            else:
                log_time = log.date

            if project.first_log is None:
                project.first_log = log

            elif log_time < project.first_log.date:
                # Encountered a log referencing the project before it's
                # set create date, update with new information
                #setattr(project, 'create_date', log.date)
                project.first_log = log

            if project.owner == cls.unknown_owner:
                # Found a reference to the project with an unknown owner
                # update with new information
                project.owner = username
        except cls.DoesNotExist:
            project = cls(
                project_id=int(project_id),
                owner=username,
                first_log=log)
        return project

    def get_access_locations(self):

        """ Returns locations that have operated on a project.
            :return: Returns a tuple -> (api locations, project log locations)"""

        from data_manager.models import ProjectLog, API_Call, Location

        # Obtain all logs which have operated on this project
        api_calls = API_Call.objects.filter(project=self)
        project_logs = ProjectLog.objects.filter(project=self)

        api_call_sources = api_calls.values_list('log__source').distinct()
        project_log_sources = project_logs.values_list('log__source').distinct()

        api_locations = Location.objects.filter(source__in=api_call_sources).exclude(country=None)
        project_log_locations = Location.objects.filter(source__in=project_log_sources).exclude(country=None)

        return (api_locations, project_log_locations)

    def logs_before_first_log(self, **extra_kwargs):
        """
        This method gets logs that are before the first_log seen by the projects
        instance. Any additional filter keyword arguments may also be passed in.
        """
        date_index = self.first_log.date
        possible_logs = Log.objects.filter(date__lte=date_index, **extra_kwargs)
        return possible_logs

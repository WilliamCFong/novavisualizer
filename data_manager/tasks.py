import gc
import logging
import re

from celery import shared_task
from django.contrib.sessions.backends.db import SessionStore
from django.db import transaction
from django.db.utils import DataError

from data_manager.utils import (chunkify_jobs, constrict_log_list,
                                construct_log_info, conv_raw_to_dict,
                                get_nested_item, link_projects,
                                resolve_project)

from .models import (API_Call, Job, LinkStep, Location, Log, Project,
                     ProjectLog, Remix, Source)

logger = logging.getLogger(__name__)

@shared_task(name='ProcessLocations_local_db')
def resolve_locations(ip_addresses):
    """Attempts to resolve the given ip addresses to physical locations.
    :ip_addresses: A list of ip addresses (strings) to attempt to resolve.
    """
    from geolite2 import geolite2
    reader = geolite2.reader()

    hits = {}
    for ip_address in ip_addresses:
        result = reader.get(ip_address)
        if result:
            hits[ip_address] = result

    # We have a dictionary of ip_addresses to successfully 'resolved' locations
    # We need to further check each result to ensure it matches the QoA of the
    # Location model
    for ip_address, loc in hits.items():
        opts = {}
        opts['source'] = Source.objects.get(pk=ip_address)
        opts['city'] = get_nested_item(loc, 'city__names__en')
        opts['country'] = get_nested_item(loc, 'country__names__en')
        opts['latitude'] = get_nested_item(loc, 'location__latitude')
        opts['longitude'] = get_nested_item(loc, 'location__longitude')
        opts['accuracy'] = get_nested_item(loc, 'location__accuracy_radius')

        if 'subdivisions' in loc:
            opts['region'] = loc['subdivisions'][0]['names']['en']
        else:
            opts['region'] = None


        if opts['city'] is not None:
            # Passes minimum QoA
            loc, _ = Location.objects.update_or_create(**opts)
            loc.save()
        elif opts['latitude'] is not None:
            # Possible that we can recover
            # TODO Do reverse geocode lookup on Google or something
            continue
        else:
            # Does not meet QoA
            continue



@shared_task(name='ChunkProcessJobs')
def job_chunk_process(job_ids):
    buffersize = 10**6  # Make buffersize 1 million entries

    # Load source cache
    source_cache = Source.objects.in_bulk()
    # Load project cache
    project_cache = Project.objects.in_bulk()

    log_cache = []
    project_dependents = []
    api_calls = []
    project_logs = []

    try:
        cur_log_id = Log.objects.all().order_by('-id')[0].pk + 1
    except IndexError:
        # No logs!
        cur_log_id = 0

    for chunk, job in chunkify_jobs(job_ids, buffersize):
        new_sources = []
        for entry in chunk:
            log_dict = conv_raw_to_dict(entry)
            ip = log_dict['source']

            # Try and resolve using cache
            try:
                source = source_cache[ip]
            except KeyError:
                source = Source(ip)
                source_cache[ip] = source
                source.save()
                new_sources.append(ip)

            log_dict['source'] = source
            log_dict['from_job'] = job

            # Clean data
            log_dict['request_url'] = log_dict['request_url'][:Log.URL_MAX_LEN]
            log_dict['referer'] = log_dict['referer'][:Log.REF_MAX_LEN]
            log_dict['user_agent_http'] = log_dict['user_agent_http'][:Log.USR_AGENT_MAX_LEN]
            log_dict['method'] = log_dict['method'][:Log.METHOD_MAX_LEN]

            # Generate the log object
            log = Log(**log_dict, id=cur_log_id)
            log_cache.append(log)

            # Determine if log is an api call or project log
            log_info = construct_log_info(log)

            if log_info is None:
                # Log does not operate on a project, continue
                cur_log_id += 1
                continue

            log_info['log'] = log
            project_id = log_info['project']
            project, is_cache_hit = resolve_project(
                    project_id,
                    log,
                    project_cache,
                    log_info['owner'])

            log_info['project'] = project

            if project and not is_cache_hit:
                project_dependents.append(project)

            log_info.pop('owner', None)

            if 'action' in log_info:
                # Log is a project log
                project_logs.append(ProjectLog(**log_info))
            else:
                # Log is an api call
                api_calls.append(API_Call(**log_info))
            cur_log_id += 1

        # Dump caches
        try:
            Log.objects.bulk_create(log_cache, batch_size=50000)
            Project.objects.bulk_create(project_dependents)
            ProjectLog.objects.bulk_create(project_logs)
            API_Call.objects.bulk_create(api_calls)
            resolve_locations(new_sources)
        except DataError:
            logger.error('Encountered error!')
            with open('log_cache_dump.txt', 'wt') as f:
                [f.write(str(log)) for log in log_cache]
            with open('project_cache_dump.txt', 'wt') as f:
                [f.write(str(project)) for _, project in project_cache.items()]
            with open('project_logcache_dump.txt', 'wt') as f:
                [f.write(str(projectlog)) for project_log in project_logs]
            with open('api_call_dump.txt', 'wt') as f:
                [f.write(str(api_call)) for api_call in api_calls]

        log_cache = []
        project_dependents = []
        project_logs = []
        api_calls = []
        gc.collect()
       
    # Assign job dates
    for job in Job.objects.filter(pk__in=job_ids).all():
        try:
            logs = Log.objects.filter(from_job=job)
            job.earliest_log_date = logs.earliest('date').date
            job.latest_log_date = logs.latest('date').date
        except Log.DoesNotExist:
            job.latest_log_date = job.earliest_log_date
        job.status = Job.FINISHED
        job.message = 'Finished. Contains %d logs.' % logs.count()
        job.save()

    # Write to disk
    logger.info('Writing remaining caches...')
    Log.objects.bulk_create(log_cache)
    Project.objects.bulk_create(project_dependents)
    ProjectLog.objects.bulk_create(project_logs)
    API_Call.objects.bulk_create(api_calls)

    #TODO Remove into separate task
    next_linkstep = LinkStep()
    next_linkstep.save()
    link_projects(next_linkstep.pk)

@shared_task(name='Linker')
def link():
    logger.info('Attempting to link projects.')
    next_linkstep = LinkStep()
    next_linkstep.save()
    link_wrapper.apply_async(args=[next_linkstep.pk])
    return next_linkstep.pk

@shared_task(name='LinkWrapper')
def link_wrapper(next_linkstep_pk):
    logger.info('Linking projects using Linkstep[{}]'.format(next_linkstep_pk))
    link_projects(next_linkstep_pk)

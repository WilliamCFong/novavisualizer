from pyproj import Proj, transform

def to_mercator(latitudes, longitudes):
    in_proj = Proj(init='epsg:4326')
    out_proj = Proj(init='epsg:3857')

    return transform(in_proj, out_proj, longitudes, latitudes)

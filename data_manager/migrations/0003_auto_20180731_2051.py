# Generated by Django 2.0.5 on 2018-07-31 20:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('data_manager', '0002_auto_20180731_1931'),
    ]

    operations = [
        migrations.AlterField(
            model_name='log',
            name='request_url',
            field=models.URLField(blank=True, default='/', max_length=3000),
        ),
    ]

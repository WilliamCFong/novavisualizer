import re

from datetime import datetime
from django.db import models

from data_manager.models import Job, Source


class Log(models.Model):

    REF_MAX_LEN = 3000
    URL_MAX_LEN = 3000
    USR_AGENT_MAX_LEN = 300
    METHOD_MAX_LEN = 100

    source = models.ForeignKey(Source, on_delete=models.CASCADE)
    date = models.DateTimeField()
    from_job = models.ForeignKey(Job, on_delete=models.CASCADE, null=True)

    method = models.CharField(max_length=METHOD_MAX_LEN)
    referer = models.CharField(max_length=REF_MAX_LEN, null=True)
    request_url = models.URLField(max_length=URL_MAX_LEN, blank=True, default='/')
    response_byte_size = models.IntegerField()
    response_code = models.SmallIntegerField()
    user_agent_http = models.CharField(max_length=USR_AGENT_MAX_LEN, null=True)

    class Meta:
        ordering = ['date']

    def __str__(self):
        return self.to_apache_str()

    @classmethod
    def from_dict(
            cls,
            log_dict,
            from_job=None,
            translator={
                    'SOURCE': 'remote_ip',
                    'DATE': 'timestamp',
                    'METHOD': 'method',
                    'REFERER': 'referer',
                    'BYTE_SIZE': 'response_byte_size',
                    'RESPONSE_CODE': 'response_code',
                    'URL': 'request_url',
                    'UA_HTTP': 'user_agent_http',
                }):

        source = Source.objects.get(ip_address=log_dict[translator['SOURCE']])
        ref_max_len = cls._meta.get_field('referer').max_length
        req_max_len = cls._meta.get_field('request_url').max_length
        user_max_len = cls._meta.get_field('user_agent_http').max_length

        log = cls(
            source=source,
            from_job=from_job,
            date=log_dict[translator['DATE']],
            method=log_dict[translator['METHOD']],
            referer=log_dict[translator['REFERER']][:ref_max_len],
            response_byte_size=log_dict[translator['BYTE_SIZE']],
            response_code=log_dict[translator['RESPONSE_CODE']],
            request_url=log_dict[translator['URL']][:req_max_len],
            user_agent_http=log_dict[translator['UA_HTTP']][:user_max_len])

        return log

    def to_apache_str(self):
        # Ignore Referer string
        str_date = datetime.strftime(self.date, '%d/%b/%Y:%H:%M:%S %z')
        return '%s - - [%s] "%s %s HTTP/1.1" %d %d "-" "%s"' % (
            self.source.ip_address,
            str_date,
            self.method,
            self.request_url,
            self.response_code,
            self.response_byte_size,
            self.user_agent_http
        )

    def is_api_call(self):
        api_check = r'/api/\w+/\d+/\d*'
        return re.search(api_check, self.request_url)

    def is_remix_call(self):
        remix_check = r'/api/remix/\d+/\d*'
        return re.search(remix_check, self.request_url)

    def is_new_project_call(self):
        new_project_check = r'/[\w\$\.\!\+\-\*\(\)\,]+/projects/new/'
        return re.search(new_project_check, self.request_url)

    def is_project_log(self):
        project_log_check = r'/[\w\$\.\!\+\-\*\(\)\,]+/projects/.*'
        return re.search(project_log_check, self.request_url)

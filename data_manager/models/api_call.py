import re

from django.db import models

from data_manager.models import Log, Project


class API_Call(models.Model):
    log = models.ForeignKey(Log, on_delete=models.CASCADE)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    call_type = models.CharField(max_length=15)
    project_revision = models.SmallIntegerField(null=True)

    def __str__(self):
        return '%d[%d] %s %d' % (self.project.pk, self.project_revision, self.call_type, self.log.pk)

    @classmethod
    def find_api_calls(cls):
        logs = Log.objects.all()
        for log in logs.iterator(chunk_size=10000000):
            result = cls.create_api_from_log(log)
            if result:
                yield result

    @classmethod
    def create_from_log(cls, log):
        extract = re.compile(r'/api/(\w+)/(\d+)/(\d+)?')
        match = extract.match(log.request_url)

        if match is None:
            return

        groups = match.groups()

        if len(groups) != 3:
            call_type, project_id = groups
            revision = 0
        else:
            call_type, project_id, revision = groups

        # Attempt to resolve the project ID that is being referenced:
        # /api/call_type/PROJECT_ID/
        if Project.objects.filter(project_id=project_id).exists():
            project = Project.objects.get(project_id=project_id)
        else:
            # If the project doesn't exist, create a placeholder with an
            # unknown owner
            project = Project(
                project_id=project_id,
                owner=Project.unknown_owner,
                first_log=log)
            project.save()

        api = cls(
            log=log,
            project=project,
            call_type=call_type,
            project_revision=revision
        )
        return api

from datetime import datetime

from django.test import Client, TestCase, override_settings
from django.utils import timezone

import data_manager.models as m
import data_manager.tests.testutils as tu


def check_log_against_dict(log, dict_ref):
    assert log.source.ip_address == dict_ref['remote_ip']
    assert log.date == dict_ref['timestamp']
    assert log.method == dict_ref['method']
    assert log.referer == dict_ref['referer']
    assert log.response_byte_size == dict_ref['response_byte_size']
    assert log.response_code == dict_ref['response_code']
    assert log.request_url == dict_ref['request_url']
    assert log.from_job == dict_ref['from_job']

class LogUnitTests(TestCase):

    def test_log_manual_creation(self):
        src = m.Source(ip_address='1.1.1.1')
        src.save()
        date = datetime.strptime(
            '24/Jun/2018:02:38:52 -0400',
            '%d/%b/%Y:%H:%M:%S %z')
        log = m.Log(
            source=src,
            date=date,
            from_job=None,
            method='GET',
            referer='Referer string',
            response_byte_size=1000,
            response_code=200,
            request_url='/',
            user_agent_http='user_agent_string'
        )
        log.save()
        assert m.Log.objects.filter(source=src)[0] == log

    def test_log_dict_creation(self):
        m.Source('1.1.1.1').save()
        log_dict = tu.create_log_dict('1.1.1.1')
        log = m.Log.from_dict(log_dict, from_job=log_dict['from_job'])
        check_log_against_dict(log, log_dict)

    def test_check_against_verylongreferer(self):
        m.Source('1.1.1.1').save()
        log_dict = tu.create_log_dict('1.1.1.1')
        log_dict['referer'] = 'Too long' * 3000
        log = m.Log.from_dict(log_dict, from_job=log_dict['from_job'])

        assert log.referer != log_dict['referer']
        assert log.referer == log_dict['referer'][:m.Log._meta.get_field('referer').max_length]

    def test_check_against_verylongrequest(self):
        m.Source('1.1.1.1').save()
        log_dict = tu.create_log_dict('1.1.1.1')
        log_dict['request_url'] = '/resource' * 3000
        log = m.Log.from_dict(log_dict, from_job=log_dict['from_job'])

        assert log.request_url != log_dict['request_url']
        assert log.request_url == log_dict['request_url'][:m.Log._meta.get_field('request_url').max_length]

    def test_check_against_verylonguseragent(self):
        m.Source('1.1.1.1').save()
        log_dict = tu.create_log_dict('1.1.1.1')
        log_dict['user_agent_http'] = 'UserAgentTooLong' * 3000
        log = m.Log.from_dict(log_dict, from_job=log_dict['from_job'])

        assert log.user_agent_http != log_dict['user_agent_http']
        assert log.user_agent_http == log_dict['user_agent_http'][:m.Log._meta.get_field('user_agent_http').max_length]

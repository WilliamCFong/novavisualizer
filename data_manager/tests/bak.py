import glob
import os
import tempfile
from datetime import datetime
from unittest.mock import patch

from django.contrib.auth.models import User
from django.core.files import File as FileWrapper
from django.db import transaction
from django.test import Client, TestCase, override_settings
from django.urls import reverse

import data_manager.models as m
import data_manager.utils as u
from data_manager.forms import LogFileForm
from data_manager.tasks import job_tasks

from .testutils import write_files, write_mangled_files


def make_log_dict(user='user', project='100', remote_ip='1.1.1.1'):
    u.source_from_dict({'remote_ip': remote_ip})
    time = datetime.strptime('24/Jun/2018:02:38:52 -0400', '%d/%b/%Y:%H:%M:%S %z')
    log = {
        'remote_ip': remote_ip,
        'timestamp': time,
        'method': 'GET',
        'referer': 'A referer string',
        'request_url': '/%s/projects/%s/' % (user, project),
        'response_byte_size': '1000',
        'response_code': '200',
        'user_agent_http': 'Semrush Botmotyo'
    }

    return log


class DataManagertestFileProcessing(TestCase):

    def test_create_source(self):
        u.source_from_dict({'remote_ip': '1.1.1.1'})
        assert m.Source.objects.filter(ip_address='1.1.1.1').exists()

    def test_create_log(self):
        time = datetime.strptime('24/Jun/2018:02:38:52 -0400', '%d/%b/%Y:%H:%M:%S %z')
        log = make_log_dict()
        log = u.log_from_dict(log, None)
        assert m.Log.objects.filter(pk=log.pk).exists()

    def test_create_location(self):
        u.source_from_dict({'remote_ip': '1.1.1.1'})
        location = {
                'remote_ip': '1.1.1.1',
                'city': 'Metropolis',
                'subdivisions': 'Supermanstan',
                'country': 'United States',
                'latitude': '-34.1',
                'longitude': '19.2',
                'accuracy': '100.0'
                }
        u.location_from_dict(location)
        assert m.Location.objects.filter(source='1.1.1.1').exists()

    def test_create_project(self):
        time = datetime.strptime('24/Jun/2018:02:38:52 -0400', '%d/%b/%Y:%H:%M:%S %z')
        log = make_log_dict()
        log = u.log_from_dict(log, None)
        u.generate_project(m.Log.objects.get(pk=log.pk))
        assert m.Project.objects.filter(project_id='100').exists()
        assert m.Project.objects.get(project_id='100').create_date == time
        assert m.Project.objects.get(project_id='100').owner == 'user'

    def test_project_update(self):
        old_time = datetime.strptime('24/Jun/2018:02:38:52 -0400', '%d/%b/%Y:%H:%M:%S %z')
        new_time = datetime.strptime('23/Jun/2018:02:38:52 -0400', '%d/%b/%Y:%H:%M:%S %z')
        old_log = make_log_dict()
        new_log = make_log_dict()
        old_log['timestamp'] = old_time
        old_log['request_url'] = '/api/blocks/100/'
        new_log['timestamp'] = new_time

        u.log_from_dict(old_log, None)
        assert m.Project.objects.get(project_id='100').owner == '--/UNKNOWN/--'
        u.log_from_dict(new_log, None)

        assert m.Project.objects.get(project_id='100').create_date == new_time
        assert m.Project.objects.get(project_id='100').owner == 'user'

    def test_api(self):
        log = make_log_dict()
        log['request_url'] = '/api/autosave/100/'
        log = u.log_from_dict(log, None)

        assert m.Log.objects.filter(pk=log.pk).exists()
        assert m.Project.objects.get(project_id='100').owner == '--/UNKNOWN/--'

        log = m.Log.objects.get(pk=log.pk)

        assert m.API_Call.objects.filter(log=log).exists()
        assert m.API_Call.objects.get(log=log).project == m.Project.objects.get(project_id='100')

    @override_settings(MEDIA_ROOT=tempfile.gettempdir())
    def test_job_processing(self):
        files = write_files()
        user = User.objects.create_user('admin', 'admin@user.com', 'secret')
        jobs = [m.Job(job_file=job, user=user) for job in files]

        for job in jobs:
            assert job.status is m.Job.UNPROCESSED

        with transaction.atomic():
            u.process_jobs(jobs)

        for job in jobs:
            assert job.status is m.Job.FINISHED

        addresses = ['1.1.1.1', '1.1.1.2', '1.1.1.3', '1.1.2.1', '1.1.2.2',
                     '1.1.2.3', '1.1.3.1', '1.1.3.2', '1.1.3.3']
        for addr in addresses:
            assert m.Source.objects.filter(ip_address=addr).exists()
            assert m.Location.objects.filter(source=addr).exists()

        for job in jobs:
            assert m.Log.objects.filter(from_job=job.pk).count() == 3

        assert m.Project.objects.count() == 3
        assert m.ProjectLog.objects.count() == 9
        assert m.Project.objects.get(project_id=1).owner == 'user1'

    @override_settings(MEDIA_ROOT=tempfile.gettempdir())
    def test_against_bad_files(self):
        files = write_mangled_files(100)
        user = User.objects.create_user('admin', 'admin@user.com', 'secret')
        jobs = [m.Job(job_file=job, user=user) for job in files]

        for job in jobs:
            assert job.status is m.Job.UNPROCESSED

        u.process_jobs(jobs)

        for job in jobs:
            assert job.status is m.Job.ERROR

    @override_settings(MEDIA_ROOT=tempfile.gettempdir())
    def test_for_very_long_files(self):
        files = [tempfile.NamedTemporaryFile(mode='w+b')]
        files[0].write(bytes('1.1.1.1 - - [24/Jun/2018:02:38:52 -0400] "' +
                       'GET ' +
                       'https://www.bad.com/' + 'MANGLED' * 3000 +
                       '" 500 10000' +
                       ' "' + 'REFERER' * 3000 + '"' +
                       ' "agentstr"', 'utf-8'))
        files[0].seek(0)
        user = User.objects.create_user('admin', 'admin@user.com', 'secret')
        jobs = [m.Job(job_file=FileWrapper(job), user=user) for job in files]

        for job in jobs:
            assert job.status is m.Job.UNPROCESSED

        u.process_jobs(jobs)

        for job in jobs:
            assert job.status is m.Job.FINISHED

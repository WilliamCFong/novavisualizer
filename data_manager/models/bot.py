import mmh3
from django.db import models
from data_manager.models import Source

class BotFilter(models.Model):
    """This model wraps UserAgent strings using a Murmur3 hash. All incoming
    logs' user agent strings will be checked against these bot filters. Any
    source which has bot traffic will be attached to the Bot model for easy
    queries.

        --- Remember that Murmur3 hash is NOT a cryptographic hash. It is NOT secure ---
    """
    bot_hash = models.BinaryField(max_length=32, primary_key=True)
    user_agent = models.CharField(max_length=1000, unique=True)

    def save(self, *args, **kwargs):
        """Saves the model and generates the hash"""
        self.bot_hash = mmh3.hash_bytes(self.user_agent)
        self.full_clean()
        super().save(*args, **kwargs)


class Bot(models.Model):
    """This model designates sources which have been identified as Bots."""
    source = models.ForeignKey(Source, on_delete=models.CASCADE)

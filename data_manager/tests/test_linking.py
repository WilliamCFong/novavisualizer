from django.test import TestCase
from data_manager.utils import link_projects, constrict_log_list
from data_manager.tests.testutils import make_log, make_project_lifetime
from data_manager.models import Project, Remix
from django.utils import timezone

from datetime import datetime, timedelta


class LogConstrictingUnitTesting(TestCase):
    """This tests for the correctness of the constrict_log_list method"""
    def test_list_time_constrict(self):
        now = timezone.now()
        # Make 100 logs
        logs = make_project_lifetime(12345, 'owner', n_access=49)
        results = constrict_log_list(logs, now + timedelta(seconds=25), '1.1.1.1', owner=None)

        self.assertEqual(len(logs[:25]), len(results))
        self.assertListEqual(logs[:25], results)

    def test_list_ip_constrict(self):
        now = timezone.now()
        lifetime1 = make_project_lifetime(12345, 'owner', ip_address='1.1.1.1')
        lifetime2 = make_project_lifetime(12346, 'owner2', ip_address='1.1.1.2')

        results = constrict_log_list(lifetime1 + lifetime2, now + timedelta(seconds=24), '1.1.1.1', owner=None)

        self.assertListEqual(lifetime1, results)


class LinkerUnitTesting(TestCase):

    """This class provides all the unit tests on linking projects to the
    log which created them."""

    def test_link_project_log(self):
        """
            Test that we can link a ProjectLog with a 'new' action to a project.
        """

        now = timezone.now()
        lifetime = make_project_lifetime(12345, 'owner')

        link_projects()

        ref_project = Project.objects.get(project_id=12345)

        self.assertEqual(ref_project.first_log, lifetime[0].log)

    def test_link_remix(self):
        """
            Test that we can link an API remix call to the correct project.
        """
        lifetime = make_project_lifetime(12345, 'owner', first_log_type='remix')

        link_projects()

        child_project = Project.objects.get(project_id=12345)
        base_project = Project.objects.get(project_id=23456)

        self.assertTrue(Remix.objects.filter(base_project=base_project, child_project=child_project).exists())

    def test_for_multiple_lifetimes(self):
        """
            Test that we can properly link multiple projects 
        """

        lifetime1 = make_project_lifetime(12345, 'owner')
        lifetime2 = make_project_lifetime(12346, 'owner')
        link_projects()

        # Refresh updates
        [x.refresh_from_db() for x in lifetime1]
        [x.refresh_from_db() for x in lifetime2]

        p_1 = Project.objects.get(project_id=12345)
        p_2 = Project.objects.get(project_id=12346)

        self.assertEqual(p_1.first_log, lifetime1[0].log)
        self.assertEqual(p_2.first_log, lifetime2[0].log)


    def test_for_multiple_remixes(self):
        """
            Test that we can properly link multiple remixes.
        """
        remix_proj_id = 23456
        make_project_lifetime(12345, 'owner', first_log_type='remix')
        make_project_lifetime(12346, 'owner', first_log_type='remix')
        make_project_lifetime(12347, 'owner', first_log_type='remix')

        link_projects()

        self.assertEqual(
            Remix.objects.filter(
                base_project=remix_proj_id).count(),
            3
        )

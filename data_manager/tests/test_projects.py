from datetime import timedelta

from django.test import TestCase
from django.utils import timezone

from data_manager.models import Log, Project, Source
from data_manager.tests.testutils import (create_log_dict, make_project_access,
                                          write_project_lifetime)


class ProjectUnitTests(TestCase):

    def test_project_creation(self):
        Source(ip_address='1.1.1.1').save()
        log = Log.from_dict(create_log_dict(url='/user/projects/100/'))
        log.save()

        [p for p in Project.generate_projects()]
        assert Project.objects.filter(pk=100).exists()
        assert Project.objects.get(pk=100).owner == 'user'
        assert Project.objects.get(pk=100).first_log == log

    def test_project_creation_on_api_log(self):
        Source(ip_address='1.1.1.1').save()
        Log.from_dict(create_log_dict(url='/api/blocks/100/')).save()
        [p for p in Project.generate_projects()]

        assert Project.objects.filter(pk=100).exists()
        assert Project.objects.get(pk=100).owner == Project.unknown_owner

    def test_against_bad_project_creation(self):
        Source(ip_address='1.1.1.1').save()
        log = Log.from_dict(create_log_dict(url='/not/a/good/url/'))
        log.save()

        projects = [p for p in Project.generate_projects()]
        assert len(projects) == 0

    def test_project_update_time(self):
        Source(ip_address='1.1.1.1').save()
        first_log = Log.from_dict(create_log_dict(url='/user/projects/100/'))
        first_log.save()

        [p for p in Project.generate_projects()]
        project = Project.objects.get(pk=100)
        assert project.first_log == first_log

        # Simulate a log encountered out of place but displaying a request
        # to a Project earlier than was known.
        later_log = Log.from_dict(create_log_dict(
            url='/user/projects/100/',
            date=timezone.now() - timedelta(hours=1)))
        later_log.save()

        [p for p in Project.generate_projects()]
        project = Project.objects.get(pk=100)
        assert project.first_log == later_log

    def test_project_update_owner(self):
        Source(ip_address='1.1.1.1').save()
        Log.from_dict(create_log_dict(url='/api/blocks/100/')).save()
        [p for p in Project.generate_projects()]

        assert Project.objects.filter(pk=100).exists()
        assert Project.objects.get(pk=100).owner == Project.unknown_owner

        # Simulate later encounter
        new = Log.from_dict(create_log_dict(
            url='/user1/projects/100/edit/')).save()
        [p for p in Project.generate_projects(new)]

        assert Project.objects.filter(pk=100).exists()
        assert Project.objects.get(pk=100).owner == 'user1'

    def test_logs_before_first_log(self):
        Source(ip_address='1.1.1.1').save()
        timeref = timezone.now()

        logs_before_ref = [
            Log.from_dict(
                create_log_dict(url='/some/url/blah',
                                date=timeref - timedelta(seconds=i + 1))
            ) for i in range(10)
        ]
        first_log = Log.from_dict(create_log_dict(
            url='/user/projects/100/', date=timeref))
        logs_after_ref = [
            Log.from_dict(
                create_log_dict(url='/some/url/bloop',
                                date=timeref + timedelta(seconds=i + 1))
            ) for i in range(10)
        ]

        [log.save() for log in logs_after_ref + [first_log] + logs_before_ref]

        Project.update_project_on_log(first_log)

        logs_before = set(Project.objects.get(pk=100).logs_before_first_log())

        assert logs_before.issubset(set(logs_before_ref + [first_log]))
        assert logs_before.isdisjoint(set(logs_after_ref))

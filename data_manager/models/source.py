from django.db import models


class Source(models.Model):
    ip_address = models.GenericIPAddressField(primary_key=True)

    @classmethod
    def from_dict(
        cls,
        src_dict,
        translator={
            'SOURCE': 'remote_ip'
        }):
        try:
            source = cls.objects.get(ip_address=src_dict[translator['SOURCE']])
        except cls.DoesNotExist:
            source = cls(ip_address=src_dict[translator['SOURCE']])
        return source

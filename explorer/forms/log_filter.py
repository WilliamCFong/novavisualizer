from django_filters import FilterSet, OrderingFilter

from data_manager.models import Log


class Log_FilterForm(FilterSet):

    def order_method(self, queryset, name, value):
        value = value[0]
        is_reversed = value[0] == '-'
        if is_reversed:
            ordering = value[1:]
        else:
            ordering = value

        # Order
        qs = order_methods[ordering](queryset)

        if is_reversed:
            qs = qs.reverse()
        return qs

    ordering = OrderingFilter(
        fields=['date', 'response_byte_size', 'user_agent_http'],
        field_labels={
            'date': 'By Date',
            'response_byte_size': 'Server Response Byte Size',
            'user_agent_http': 'User Agent String'
        }
    )

    class Meta:
        model = Log
        fields = {
            'source__ip_address': ['exact'],
            'date': ['gte', 'lte'],
            'request_url': ['contains'],
            'response_byte_size': ['exact', 'gte', 'lte'],
            'response_code': ['exact'],
            'user_agent_http': ['exact', 'contains']
        }

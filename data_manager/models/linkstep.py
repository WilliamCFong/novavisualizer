from django.db import models


class LinkStep(models.Model):

    """A linkstep encompasses a single user project-link action."""
    created_on = models.DateTimeField(auto_now_add=True)

    ROLLED_BACK = 'RB'
    ACTIVE = 'AC'

    STATUSES = (
        (ROLLED_BACK, 'Rolled Back'),
        (ACTIVE, 'Active')
    )

    status = models.CharField(max_length=2, choices=STATUSES, default=ACTIVE)


    def rollback(self, dry_run=False):
        """Rolls back changes made during this linkstep. Applying
        a rollback again will simply change the linkstep back to being
        active. active -> rolledback -> active etc.

        :dry_run: If false then changes will be made to the database.
        :returns: A mapping of Remixes and NewActions querysets which
        were/would be affected by the rollback."""

        from data_manager.models import Remix, NewAction

        relevant_remixes = Remix.objects.filter(linkstep=self)
        relevant_newactions = NewAction.objects.filter(linkstep=self)
        if not dry_run:
            if self.status == LinkStep.ROLLED_BACK:
                self.status = LinkStep.ACTIVE
            else:
                self.status = LinkStep.ROLLED_BACK

            self.save()

        return {Remix: relevant_remixes, NewAction: relevant_newactions}
import re
from datetime import datetime
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from django.db import models, transaction
from hashlib import sha256

from data_manager.models import Location, Source


def check_collision(date):
    from data_manager.models import Log
    for job in Job.objects.filter(status=Job.FINISHED):
        if job.latest_log_date is None:
            try:
                job.latest_log_date = Log.objects.filter(from_job=job).latest('date').date
            except Log.DoesNotExist:
                job.latest_log_date = job.earliest_log_date
        # Check to see if the given date resides within any of the ranges, if
        # it does, raise a ValidationError
        # TODO Make less strict
        # if job.earliest_log_date < date < job.latest_log_date:
        #     # Date resides within another job's data range, raise an error.
        #     raise ValidationError(_(
        #         'Given job date %(given_date)s resides within job %(job_id)s: '
        #         '%(job_early)s to %(job_late)s.'),
        #         params={
        #             'given_date': date,
        #             'job_id': job.pk,
        #             'job_early': job.earliest_log_date,
        #             'job_late': job.latest_log_date
        #         }
        #     )

def digest_file(fp, digest_size=10000):
        m = sha256()
        while True:
            buffer = fp.read(digest_size)
            if not buffer:
                break
            m.update(buffer)
        fp.seek(0)
        return m.digest()


class Job(models.Model):
    upload_date = models.DateTimeField(auto_now_add=True)
    job_file = models.FileField()
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

    UNPROCESSED = 'UN'
    PROCESSING = 'PR'
    FINISHED = 'FI'
    ERROR = 'ER'

    STATUSES = (
            (UNPROCESSED, 'Unprocessed'),
            (PROCESSING, 'Processing'),
            (FINISHED, 'Finished'),
            (ERROR, 'Error')
            )

    status = models.CharField(max_length=2, choices=STATUSES, default=UNPROCESSED)
    message = models.TextField(default='File has not been processed yet')
    checksum = models.BinaryField(max_length=32, unique=True, editable=False)

    # Job files should never 'collide'
    earliest_log_date = models.DateTimeField(unique=True, validators=[check_collision])
    latest_log_date = models.DateTimeField(blank=True, null=True, validators=[check_collision])

    def save(self, *args, **kwargs):
        # Peek at first line
        if self.earliest_log_date is None:
            try:
                log_str = self.job_file.readline()
                first_date = re.match(r'[^\[]+\[([^\]]+)\]', str(log_str, 'utf-8')).groups()[0]
                first_date = datetime.strptime(first_date, '%d/%b/%Y:%H:%M:%S %z')
            except AttributeError:
                # Empty file?
                first_date = timezone.now()

            self.earliest_log_date = first_date
        # Check collision
        self.clean_fields()


        self.job_file.seek(0)
        self.checksum = digest_file(self.job_file)

        # Attempt to save, will through IntegrityError.
        super(Job, self).save(*args, **kwargs)

from django_filters import FilterSet, CharFilter

from data_manager.models import Location


class LocationFilterForm(FilterSet):

    class Meta:
        model = Location
        fields = ['country', 'region', 'city']

    country = CharFilter(field_name='country', lookup_expr='icontains')
    region = CharFilter(field_name='region', lookup_expr='icontains')
    city = CharFilter(field_name='city', lookup_expr='icontains')

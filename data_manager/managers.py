from django.db.models import Manager

from data_manager.models import LinkStep

class RollbackManager(Manager):
    """This manager wraps querysets on Models which respect LinkSteps.
    It does this by providing overriding the interface 'get_queryset' and
    filtering it by only returning models that have an ACTIVE LinkStep"""

    def get_queryset(self):
        """Filters results by only returning active models"""
        return super().get_queryset().filter(linkstep__status=LinkStep.ACTIVE)
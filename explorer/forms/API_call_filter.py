from django_filters import FilterSet, OrderingFilter

from data_manager.models import API_Call


class API_CallFilterForm(FilterSet):

    class Meta:
        model = API_Call
        fields = {'log__source__ip_address': ['exact'],
                  'project__project_id': ['exact'],
                  'call_type': ['exact']
        }

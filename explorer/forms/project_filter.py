from django.db.models import Count
from django_filters import FilterSet, OrderingFilter

from data_manager.models import Project

ORDERBY_N_CHILDREN = ('n_children', lambda qs: qs.annotate(order_field=Count('remixed_from_remix')).order_by('order_field'))
ORDERBY_CREATE_DATE = ('create_date', lambda qs: qs.order_by('first_log__date'))
ORDERBY_N_API_CALLS = ('n_api_calls', lambda qs: qs.annotate(order_field=Count('api_call')).order_by('order_field'))
ORDERBY_N_PROJECT_LOGS = ('n_project_logs', lambda qs: qs.annotate(order_field=Count('projectlog')).order_by('order_field'))
ORDERBY_ID = ('project_id', lambda qs: qs.order_by('project_id'))

ORDER_METHODS = dict([
    ORDERBY_N_CHILDREN,
    ORDERBY_CREATE_DATE,
    ORDERBY_N_API_CALLS,
    ORDERBY_N_PROJECT_LOGS,
    ORDERBY_ID])
ORDER_LABELS = {
    ORDERBY_N_CHILDREN[0]: 'Number of Child Projects',
    ORDERBY_CREATE_DATE[0]: 'Create Date',
    ORDERBY_N_API_CALLS[0]: 'Number of API Calls',
    ORDERBY_N_PROJECT_LOGS[0]: 'Number of Project logs',
    ORDERBY_ID[0]: 'Order by Project ID'
}


class Project_FilterForm(FilterSet):

    def order_method(self, queryset, name, value):
        value = value[0]
        is_reversed = value[0] == '-'
        if is_reversed:
            ordering = value[1:]
        else:
            ordering = value

        # Order
        qs = ORDER_METHODS[ordering](queryset)

        if is_reversed:
            qs = qs.reverse()
        return qs

    def get_order_label(self):
        try:
            ordering = self.data['ordering']
            ordering = ordering[1:] if ordering[0] == '-' else ordering
            return ORDER_LABELS[ordering]
        except KeyError:
            return ''

    ordering = OrderingFilter(
        fields=['n_children', 'create_date', 'n_api_calls', 'n_project_logs', 'project_id'],
        field_labels={
            'n_children': 'Number of Child Projects',
            'create_date': 'Project creation date',
            'n_api_calls': 'Number of API Calls',
            'n_project_logs': 'Number of project logs',
            'project_id': 'Project ID'
        },
        method='order_method'
    )

    class Meta:
        model = Project
        fields = {
            'owner': ['exact', 'contains'],
            'project_id': ['exact']
        }

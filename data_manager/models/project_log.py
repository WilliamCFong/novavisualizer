import re

from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from data_manager.models import Log, Project


class ProjectLog(models.Model):
    log = models.OneToOneField(Log, on_delete=models.CASCADE)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, null=True)
    action = models.TextField(blank=True, default='edit')
    project_revision = models.IntegerField(default=0)

    def __str__(self):
        return '%d[%d] %s %d' % (self.project.pk, self.project_revision, self.action, self.log.pk)